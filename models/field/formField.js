const FormFieldSettings = require('./formFieldSettings');

class FormField {
    constructor(name, type, settings = {}) {
        const VALID_KEYS = ['mandatory', 'visible', 'readOnly', 'defaultValue'];

        if (!name || !type) {
            throw new Error('required parameters were not passed');
        }
        this.name = name;
        this.type = type;
        this.settings = settings;

        if (Object.keys(settings)) {

            for (let option in settings) {
                if (!VALID_KEYS.find(key => key == option)) {
                    delete settings[option]
                }
            }
            this.setSettings(settings);
        }
    }

    /**
     * @param {Object} options
     */
    setSettings(options = {}) {
        this.settings = new FormFieldSettings(options);
    }
}

module.exports = FormField;