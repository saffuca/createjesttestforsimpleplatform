class FormFieldSettings {
    constructor(options = {}) {
        if (Object.keys(options).length) {
            (Object.keys(options).forEach(key => this[key] = options[key]));
        }
    }
}
module.exports = FormFieldSettings;