const relatedListSettings = require('./relatedListSettings');

class RelatedList {
    constructor(name, relatedTableId, settings = {}) {
        const VALID_KEYS = ['mandatory', 'visible'];

        if (!name) {
            throw new Error('required parameters were not passed');
        }
        this.name = name;
        this.relatedTableId = relatedTableId;
        this.settings = settings;

        if (Object.keys(settings)) {

            for (let option in settings) {
                if (!VALID_KEYS.find(key => key == option)) {
                    delete settings[option]
                }
            }
            this.setSettings(settings);
        }
    }

    /**
     * @param {Object} options
     */
    setSettings(options = {}) {
        this.settings = new relatedListSettings(options);
    }
}

module.exports = RelatedList;