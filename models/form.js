const FormField = require('./field/formField');
const RelatedList = require('./relatedList/relatedList');
const Section = require('./section/section');

class Form {
    constructor(tableName, fixture = {}) {
        this.fields = [];
        this.sections = []
        this.relatedLists = [];
        this.fixture = fixture;
        this.tableName = tableName;
    }

    /**
     * @param {String} name
     * @param {String} type
     * @param {Object} options
     */
    addField(options) {
        const {
            name,
            type,
            settings
        } = options;
        this.fields.push(new FormField(name, type, settings));
    }

    /**
     * @param {String} name
     * @param {Object} options
     */
    addRelatedList(options) {
        const {
            name,
            settings
        } = options;
        this.relatedLists.push(new RelatedList(name, settings));
    }

    /**
     * @param {String} name
     * @param {Object} options
     */
    addSection(options) {
        const {
            name,
            settings
        } = options;
        this.sections.push(new Section(name, settings));
    }
}

module.exports = Form;