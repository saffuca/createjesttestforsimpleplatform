class FormFilter {
    constructor(form) {
        this.form = form;
    }

    /**
     * 
     * @returns {Array<object>}
     */
    getFieldsWithSettingMandatory() {
        if (!this.form.fields.length) {
            throw new Error('expected array of fields got empty');
        }

        return this.form.fields
            .filter(field => field.hasOwnProperty('settings') && field.settings.hasOwnProperty('mandatory'));
    }

    /**
     * 
     * @returns {Array<object>}
     */
    getFieldsWithSettingVisible() {
        if (!this.form.fields.length) {
            throw new Error('expected array of fields got empty');
        }

        return this.form.fields
            .filter(field => field.hasOwnProperty('settings') && field.settings.hasOwnProperty('visible'));
    }

    /**
     * 
     * @returns {Array<object>}
     */
    getFieldsWithSettingReadOnly() {
        if (!this.form.fields.length) {
            throw new Error('expected array of fields got empty');
        }

        return this.form.fields
            .filter(field => field.hasOwnProperty('settings') && field.settings.hasOwnProperty('readOnly'));

    }

    /**
     * 
     * @returns {Array<object>}
     */
    getFieldsWithSettingDefaultValue() {
        if (!this.form.fields.length) {
            throw new Error('expected array of fields got empty');
        }

        return this.form.fields
            .filter(field => field.hasOwnProperty('settings') && field.settings.hasOwnProperty('defaultValue'));

    }

    /**
     * 
     * @returns {Array<object>}
     */
    getSectionsWithSettingMandatory() {
        if (!this.form.sections.length) {
            throw new Error('expected array of sections got empty');
        }

        return this.form.sections
            .filter(tab => tab.hasOwnProperty('settings') && tab.settings.hasOwnProperty('mandatory'));
    }

    /**
     * 
     * @returns {Array<object>}
     */
    getSectionsWithSettingVisible() {
        if (!this.form.sections.length) {
            throw new Error('expected array of sections got empty');
        }

        return this.form.sections
            .filter(tab => tab.hasOwnProperty('settings') && tab.settings.hasOwnProperty('visible'));
    }

    /**
     * 
     * @returns {Array<object>}
     */
    getRelatedListsWithSettingMandatory() {
        if (!this.form.relatedLists.length) {
            throw new Error('expected array of related list got empty');
        }

        return this.form.relatedLists
            .filter(relatedList => relatedList.hasOwnProperty('settings') && relatedList.settings.hasOwnProperty('mandatory'));
    }


    /**
     * 
     * @returns {Array<object>}
     */
    getRelatedListsWithSettingVisible() {
        if (!this.form.relatedLists.length) {
            throw new Error('expected array of related list got empty');
        }

        return this.form.relatedLists
            .filter(relatedList => relatedList.hasOwnProperty('settings') && relatedList.settings.hasOwnProperty('visible'));
    }
}

module.exports = FormFilter;