const Form = require('./models/form');
const FormFilter = require('./tools/formFilter')
const form = new Form('sys_cmdb_ci_service');

const fields = [{
        name: 'number',
        type: 'string',
        settings: {
            readOnly: true
        }
    }, {
        name: 'name',
        type: 'string',
        settings: {
            mandatory: true,
        }
    },
    {
        name: 'service_type',
        type: 'select',
        settings: {
            defaultValue: '--None--',
        }

    }, {
        name: 'master_service',
        type: 'reference'
    }, {
        name: 'service_specification',
        type: 'reference',
    },
    {
        name: 'description',
        type: 'textarea'
    }, {
        name: 'owned_by',
        type: 'reference',
        settings: {
            mandatory: true,
        }

    }, {
        name: 'ci_status',
        type: 'select',
        settings: {
            defautValue: 'New'
        }
    },
    {
        name: 'operational_status',
        type: 'select',
        settings: {
            mandatory: true,
            defaultValue: '--None--'
        }
    }, {
        name: 'business_criticality',
        type: 'select',
        settings: {
            mandatory: true,
            defaultValue: '--None--'
        }
    }
];

for (let field of fields) {
    form.addField(field);
}

form.addRelatedList({
    name: 'relatedListName',
    settings: {
        'mandatory': true,
        'hex': '#sa'
    }
});
form.addSection({
    name: 'sectionName',
    settings: {
        'mandatory': true,
    }
});

const filter = new FormFilter(form);

function formValidationTest(form) {
    const scriptHeader = `
import { setConfigBeforeAll } from 'src/helpers/main';
import { SITE_URL } from 'src/constants/general';
import SimpleForm from 'src/helpers/simpleForm';
import SimpleList from 'src/helpers/simpleList';
jest.setTimeout(60000);
${Object.keys(form.fixture).length ? `beforeAll(setConfigBeforeAll({ page, filename: __filename }));` : 'beforeAll(setConfigBeforeAll({ page }));'}
`;

    const scriptBody = `
describe('Form Validation', () => {
    const fields = ${JSON.stringify(form.fields)};
    test('Check field set for new record', async() => {
        await page.goto(\`\${SITE_URL}record\/sys_cmdb_ci_service\`, { waitUntil: 'networkidle0' });

        for (let field of fields) {
            const fieldSelector = \`[data-test=\${field.name}-field-\${field.type}]\`;
            expect(\`\${field.name} \${!!await page.\$(fieldSelector)}\`).toBe(\`\${field.name} true\`);
        }
    });
    ${
        filter.getFieldsWithSettingMandatory(form).length
        ? `
    test('Check mandatory attribute', async() => {
        const mandatoryFields = fields.filter(field => field.hasOwnProperty('settings') && field.settings.hasOwnProperty('mandatory'));
        for (let field of mandatoryFields) {
            expect(await page.\$(\`[data-test=\${field.name}-field-\${field.type}][data-test-mandatory="\${field.settings.mandatory}"]\`)).toBeTruthy();
        }
    });`
        : ''
    }
    ${
        filter.getFieldsWithSettingReadOnly(form).length
        ? `
    test('Check readOnly attribute', async() => {
        const readOnlyFields = fields.filter(field => field.hasOwnProperty('settings') && field.settings.hasOwnProperty('readOnly'));
        for (let field of readOnlyFields) {
            expect(await page.$(\`[data-test=\${field.name}-field-\${field.type}][data-test-readonly="\${field.settings.readOnly}"]\`)).toBeTruthy();
        }
    });
        `
        : ''
    }
    ${
        filter.getFieldsWithSettingDefaultValue(form).length
        ? `
    test('Default values', async() => {
        const form = new SimpleForm(page);
        const fieldsWithDefaultValue = fields.filter(field => field.hasOwnProperty('settings') && field.settings.hasOwnProperty('defaultValue'));
        for (let field of fieldsWithDefaultValue) {
            if (field.type == 'select') {
                field.type = 'choice';
            }
            expect(await form.getValue(field.name, field.type)).toBe(field.settings.defaultValue);
        }
    });
        `
        : ''
    }
});`
    return scriptHeader + '\n' + scriptBody
}


form.fixture = {
    "test": "s"
}
console.log(form);
console.log(formValidationTest(form))